<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Backend\BackendBaseController;
use Illuminate\Http\Request;

class HomeController extends BackendBaseController
{
    protected  $panel = 'Dashboard';
    protected $view_path = 'home';
    protected $base_route = 'home';
    protected $title = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $this->title = $this->panel;
        return view($this->__loadDataToView($this->view_path ),compact('data'));

    }
}
