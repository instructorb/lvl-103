@extends('backend.layouts.master')
@section('title',$title)
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">{{$title}}</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => $base_route . 'store']) !!}
      <div class="form-group">
        {!! Form::label('name','Name') !!}
        {!! Form::text('name',null,['class' => 'form-control','placeholder' => "Enter Username"]) !!}
      </div>
      {!! Form::close() !!}
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      Footer
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->
  @endsection