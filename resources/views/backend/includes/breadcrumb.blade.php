<div class="row mb-2">
  <div class="col-sm-6">
    <h1>{{$panel}}</h1>
  </div>
  <div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item">
        @if($panel != 'Dashboard')
          <a href="{{route($base_route.'index')}}">{{$panel}}</a>
        @else
          <a href="{{route($base_route)}}">{{$panel}}</a>
        @endif
      </li>
      <li class="breadcrumb-item active">{{$title}}</li>
    </ol>
  </div>
</div>