<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('/backend')->name('backend.')->group(function(){
    Route::prefix('/category')->name('category.')->group(function(){
        Route::get('/create',[CategoryController::class,'create'])->name('create');
        Route::get('/',[CategoryController::class,'index'])->name('index');
        Route::get('/{id}',[CategoryController::class,'show'])->name('show');
        Route::get('/{id}/edit',[CategoryController::class,'edit'])->name('edit');
        Route::post('/',[CategoryController::class,'store'])->name('store');

    });
});